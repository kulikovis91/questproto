#include <SFML/Graphics.hpp>

class Character {
public:
	int x, y;

	Character() 
	{
		rect = sf::RectangleShape(sf::Vector2f(100.0f, 200.0f));
		rect.setFillColor(sf::Color::Green);
		rect.setPosition(300, 550);
	}

	void Draw(sf::RenderWindow *wnd) 
	{
		wnd->draw(rect);
	}

	void Move() 
	{

	}

	~Character() {}
private:
	//sf::Texture tex;
	//sf::Image img;
	//sf::Sprite sp;
	sf::RectangleShape rect;
	sf::Vector2i destPos;

};