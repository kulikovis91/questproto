#include <SFML/Graphics.hpp>
#include "Character.cpp"

int main()
{
    sf::RenderWindow window(sf::VideoMode(1024, 768), "SFML works!");
    
	sf::Texture bgTex;
	sf::Sprite bg;
	sf::Image img;
	Character c;
	
	bgTex.create(1440, 1008);
	img.loadFromFile("../bg.png");
	bgTex.update(img);
	bg.setTexture(bgTex);


    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
		window.draw(bg);
		c.Draw(&window);
        window.display();
    }

    return 0;
}